// script.js

let canvas = document.getElementById("canvas");

canvas.width  = 600;
canvas.height = 400;

let ctx = canvas.getContext('2d');

let up    = false; // W
let down  = false; // S
let left  = false; // A
let right = false; // D

let x = 50;
let y = 100;

let r = 0;
let g = 0;
let b = 0;

let loop = function(){

	let moved = false;

	if(up){
		y--;
		moved = true;
	}
	if(down){
		y++;
		moved = true;
	}
	if(left){
		x--;
		moved = true;
	}
	if(right){
		x++;
		moved = true;
	}

	if(moved){
		r = (r + 3) % 255;
		g = (g + 5) % 255;
		b = (b + 7) % 255;
	}

	ctx.fillStyle = "rgb(" + r + ", " + g + ", " + b + ")";

	ctx.clearRect(0,0, 600, 400);
	ctx.fillRect( // залить прямоугольник
		x, y, // x и у левого верхнего угла
		50, 50 // ширина и высота
	);

}

setInterval(loop, 1000 / 60);

document.addEventListener("keydown", function(e){
	
	if(e.code == "KeyW"){
		up = true;
	}
	if(e.code == "KeyS"){
		down = true;
	}
	if(e.code == "KeyA"){
		left = true;
	}
	if(e.code == "KeyD"){
		right = true;
	}

});

document.addEventListener("keyup", function(e){
	
	if(e.code == "KeyW"){
		up = false;
	}
	if(e.code == "KeyS"){
		down = false;
	}
	if(e.code == "KeyA"){
		left = false;
	}
	if(e.code == "KeyD"){
		right = false;
	}

});