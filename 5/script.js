// script.js

let canvas = document.getElementById("canvas");

canvas.width  = 600;
canvas.height = 400;

let ctx = canvas.getContext('2d');

let up    = false; // W
let down  = false; // S
let left  = false; // A
let right = false; // D

let x = 50;
let y = 300;

let dx = 0.2;
let dy = -10;

let gravity = 0.3;

// let r = 0;
// let g = 0;
// let b = 0;
let h = 0;

let loop = function(){


	// r = (r + 3) % 255;
	// g = (g + 5) % 255;
	// b = (b + 7) % 255;

	h = ++h % 360;

	x = x + dx;
	y = y + dy;

	dy = dy + gravity;

	let footY = y + 30;

	if(footY > 350){

		dy = dy * -0.85;

		let miss = footY - 350;

		if( Math.abs( dy ) < 3 ){
			y = 350 - 30;
			dy = 0;
		} else {
			y = 350 - miss - 30;
		}

	}
	

	// ctx.clearRect(0,0, 600, 400);

	ctx.fillStyle = "black";
	ctx.fillRect(
		0, 350,
		600, 50
	);

	ctx.fillStyle = "hsl(" + h + ", 90%, 50%)";
	ctx.beginPath();
	ctx.arc(
		x, y, // координаты центра x и y
		30, // Радиус
		0, 2 * Math.PI // Начальная и конечная точки на окружности
	);
	ctx.fill();

}

setInterval(loop, 1000 / 60);

document.addEventListener("keydown", function(e){
	
	if(e.code == "KeyW"){
		up = true;
	}
	if(e.code == "KeyS"){
		down = true;
	}
	if(e.code == "KeyA"){
		left = true;
	}
	if(e.code == "KeyD"){
		right = true;
	}

});

document.addEventListener("keyup", function(e){
	
	if(e.code == "KeyW"){
		up = false;
	}
	if(e.code == "KeyS"){
		down = false;
	}
	if(e.code == "KeyA"){
		left = false;
	}
	if(e.code == "KeyD"){
		right = false;
	}

});