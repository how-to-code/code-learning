// script.js

const GRAVITY = 0.35;

let canvas = document.getElementById("canvas");

canvas.width  = 1200;
canvas.height = 600;

let ctx = canvas.getContext('2d');

let up    = false; // W
let down  = false; // S
let left  = false; // A
let right = false; // D

let blocks    = [];
let obstacles = [];
let coins     = [];
let decors    = [];

let coinsCollected = 0;
let camera;

class Camera {

	constructor(){

		this.x = 0;
		this.y = 0;
		this.follow = null;

	}

	trackObject(obj){
		this.follow = obj;
	}

	update(){

		if(!this.follow){
			return;
		}

		let x1 = this.x + 500;
		let x2 = this.x + 700;

		if(this.follow.x + 32 < x1){
			this.x = this.follow.x + 32 - 500;
		}

		if(this.follow.x + 32 > x2){
			this.x = this.follow.x + 32 - 700;
		}

		let y1 = this.y + 200;
		let y2 = this.y + 400;

		if(this.follow.y + 32 < y1){
			this.y = this.follow.y + 32 - 200;
		}

		if(this.follow.y + 32 > y2){
			this.y = this.follow.y + 32 - 400;
		}

		if(this.y > 4){
			this.y = 4;
		}

	}

}

class Character {

	constructor(x = 0, y = 0){

		this.image = {
			right: "img/human_blue.png",
			left:  "img/human_blue_left.png",
		};
		this.x = x;
		this.y = y;
		this.dy = 0;
		this.speed = 4;
		this.jumpSpeed = 10;
		this.onFloor = false;

		this.direction = "right";

		this.texture = {
			right: new Image(),
			left:  new Image()
		};
		this.texture.left.src  = this.image.left;
		this.texture.right.src = this.image.right;

	}

	update(){

		// Гравитация
		this.onFloor = false;

		this.dy = this.dy + GRAVITY;
		this.y = this.y + this.dy;

		let footY = this.y + 64;

		// Падение на землю
		if(footY > 500){

			this.y = 500 - 64;
			this.dy = 0;
			this.onFloor = true;

		}

		// Падение на блок
		// && — это означает "И"
		// то есть ЕСЛИ персонаж НЕ на полу И движется вниз
		if(!this.onFloor && this.dy > 0){

			let footX = this.x + 32;

			let matchedBlocks = [];

			blocks.forEach(function(block){

				let leftX = block.x;
				let rightX = leftX + 64;

				if(footX >= leftX && footX <= rightX){
					matchedBlocks.push(block);
				}

			});

			let thiz = this;

			matchedBlocks.forEach(function(block){

				let currentY = footY;
				let previousY = currentY - thiz.dy;

				if (currentY >= block.y && previousY <= block.y) {

					thiz.y = block.y - 64;
					thiz.dy = 0;
					thiz.onFloor = true;

				}

			})


		}

		// Столкновение с препятствием
		let thiz = this;
		obstacles.forEach(function(obstacle){

			if(!obstacle.isActive){
				return;
			}

			let x1 = thiz.x;
			let y1 = thiz.y;
			let x2 = obstacle.x;
			let y2 = obstacle.y;

			let c2 = (x1 - x2) ** 2 + (y1 - y2) ** 2;
			let c  = Math.sqrt(c2);

			if(c < 40){
				thiz.hit();
			}
		})

		coins.forEach((coin) => {

			if(coin.collected){
				return;
			}

			let x1 = this.x + 32;
			let y1 = this.y + 32;

			let x2 = coin.x + 16;
			let y2 = coin.y + 16;

			let c2 = (x1 - x2) ** 2 + (y1 - y2) ** 2;
			let c  = Math.sqrt(c2);

			if(c < 28){
				// Персонаж коснулся монетки
				coin.collected = true;
				coinsCollected++;
			}

		})

	}

	hit(){
		this.x = 300;
		this.y = 200;
	}

	moveLeft(){
		this.x = this.x - this.speed;
		this.direction = "left";
	}

	moveRight(){
		this.x = this.x + this.speed;
		this.direction = "right";
	}

	jump(){
		if(this.onFloor){
			this.dy = this.dy - this.jumpSpeed;
		}
	}

	draw(){

		if(this.direction == "right"){

			ctx.drawImage(
				this.texture.right,
				this.x - camera.x, this.y - camera.y
			);

		} else {

			ctx.drawImage(
				this.texture.left,
				this.x - camera.x, this.y - camera.y
			);

		}

	}

}

class Ground {

	constructor(){

		this.image = "img/ground.png";
		this.x = 0;
		this.y = 500;

		this.texture = new Image();
		this.texture.src = this.image;

	}

	draw(){

		let count = Math.floor( camera.x / 1200 );

		ctx.drawImage(
			this.texture,
			1200 * count - camera.x , this.y - camera.y
		);

		ctx.drawImage(
			this.texture,
			1200 * count - camera.x + 1200, this.y - camera.y
		);
	}

}

class Sky {

	constructor(){

		this.image = "img/sky.png";
		this.x = 0;
		this.y = 0;

		this.texture = new Image();
		this.texture.src = this.image;

	}

	update(){
		this.x = this.x + 0.2;
		this.x = this.x % 1200;
	}

	draw(){
		ctx.drawImage(
			this.texture,
			this.x, this.y
		);
		ctx.drawImage(
			this.texture,
			this.x - 1200, this.y
		);
	}

}

class Block {

	constructor(x = 0, y = 0){

		this.image = "img/grass_block.png";
		this.x = x;
		this.y = y;

		this.texture = new Image();
		this.texture.src = this.image;

		blocks.push(this);

	}

	draw(){
		ctx.drawImage(
			this.texture,
			this.x - camera.x, this.y - camera.y
		);
	}

}

class Fireplace {

	constructor(x = 0, y = 0){

		this.image = {
			active: "img/fire.png",
			inactive: "img/ember.png"
		};
		this.x = x;
		this.y = y;

		this.isActive = true;

		this.framesCounter = 0;
		this.framesToChange = 100;

		this.texture = {
			active: new Image(),
			inactive: new Image()
		};

		this.texture.active.src   = this.image.active;
		this.texture.inactive.src = this.image.inactive;

		obstacles.push(this);

	}

	update(){

		this.framesCounter++;

		if(this.framesCounter > this.framesToChange){
			this.isActive = !this.isActive;
			this.framesCounter = 0;
		}

	}

	draw(){

		if(this.isActive){

			ctx.drawImage(
				this.texture.active,
				this.x - camera.x, this.y - camera.y
			);

		} else {

			ctx.drawImage(
				this.texture.inactive,
				this.x - camera.x, this.y - camera.y
			);

		}


	}

}

class Coin {

	constructor(x = 0, y = 0){

		this.x = x;
		this.y = y;

		this.image = [
			"img/coin/coin_1.png",
			"img/coin/coin_2.png",
			"img/coin/coin_3.png",
			"img/coin/coin_4.png",
		];

		this.texture = [];

		this.currentFrame = 0;
		this.animationSpeed = 10;
		this.framesCounter = 0;

		this.collected = false;

		this.image.forEach((src) => {

			let img = new Image();
			img.src = src;

			this.texture.push(img);

		});

		coins.push(this);

	}

	draw(){

		if(this.collected){
			return;
		}

		ctx.drawImage(
			this.texture[this.currentFrame],
			this.x - camera.x, this.y - camera.y
		);

		this.framesCounter++;

		if(this.framesCounter > this.animationSpeed){
			this.currentFrame++;
			this.framesCounter = 0;
		}

		this.currentFrame = this.currentFrame % this.texture.length;
	}

}

class HUD {

	constructor(x = 0, y = 0){

		this.image = "img/coin/coin_1.png";
		this.x = x;
		this.y = y;

		this.texture = new Image();
		this.texture.src = this.image;

	}

	draw(){
		ctx.drawImage(
			this.texture,
			this.x, this.y
		);

		ctx.font = "32px Arial";
		ctx.fillStyle = "white";
		ctx.fillText(
			"× " + coinsCollected,
			this.x + 32 + 10, this.y + 28
		);
		ctx.lineWidth = 1.5;
		ctx.strokeStyle = "black";
		ctx.strokeText(
			"× " + coinsCollected,
			this.x + 32 + 10, this.y + 28
		);

	}

}


class Decor {

	constructor(img, x = 0, y = 0){

		this.image = img;
		this.x = x;
		this.y = y;

		this.texture = new Image();
		this.texture.src = this.image;

		decors.push(this);

	}

	draw(){
		ctx.drawImage(
			this.texture,
			this.x - camera.x, this.y - camera.y
		);
	}

}

class Bush extends Decor {

	constructor(x = 0, y = 0){
		super("img/bush.png", x, y);
	}

}

class Tree extends Decor {

	constructor(x = 0, y = 0){
		super("img/tree.png", x, y);
	}

}

class Grass extends Decor {

	constructor(x = 0, y = 0){
		super("img/grass.png", x, y);
	}

}


let char = new Character(300, 200);
camera = new Camera();
camera.trackObject(char);

let ground = new Ground();
let sky    = new Sky();
let hud = new HUD(16, 16);

new Coin(534 + 16, 270 - 32 - 16);
new Coin(534 + 16 + 64, 270 - 32 - 16);

let k = 0;
new Block(300, 400);
new Block(370, 350);
new Block(434, 350);
new Block(534, 270);
new Block(534, 270);

new Block(534, 270 - 64);


new Block(534 + 64 * ++k, 270);
new Block(534 + 64 * ++k, 270);
new Block(534 + 64 * ++k, 270);
new Block(534 + 64 * ++k, 270);
new Block(534 + 64 * ++k, 270);
new Block(534 + 64 * ++k, 270);
new Block(534 + 64 * ++k, 270);
new Block(534 + 64 * ++k, 270);
new Block(534 + 64 * ++k, 270);
new Block(534 + 64 * ++k, 270);
new Block(534 + 64 * ++k, 270);
new Block(534 + 64 * ++k, 270);

new Fireplace(434, 350 - 64);
new Fireplace(534 + 64 * 6, 270 - 64).isActive = false;
new Fireplace(534 + 64 * 9, 270 - 64);

new Bush(550, 270 - 48);
new Bush(650, 270 - 48);
new Bush(100, 454);
new Tree(100, 308);
new Tree(720, 78);
new Grass(774, 254);
new Grass(774 + 16, 254);

new Grass(800, 484);
new Grass(840, 484);
new Grass(867, 484);
new Grass(923, 484);
new Grass(1021, 484);
new Grass(999, 484);

let loop = function(){

	if(up){
		char.jump();
	}
	if(left){
		char.moveLeft();
	}
	if(right){
		char.moveRight();
	}

	// Обновляем состояние элементов
	char.update();
	sky.update();

	obstacles.forEach(function(obstacle){
		obstacle.update();
	});

	camera.update();


	// Очищаем экран
	ctx.clearRect(0,0, 1200, 600);

	// Отрисовываем элементы на экран
	sky.draw();

	blocks.forEach(function(block){
		block.draw();
	});

	decors.forEach(function(decor){
		decor.draw();
	});

	obstacles.forEach(function(obstacle){
		obstacle.draw();
	});

	ground.draw();

	coins.forEach(function(coin){
		coin.draw();
	});

	char.draw();
	hud.draw();
}

setInterval(loop, 1000 / 60);

document.addEventListener("keydown", function(e){
	
	if(e.code == "KeyW"){
		up = true;
	}
	if(e.code == "KeyS"){
		down = true;
	}
	if(e.code == "KeyA"){
		left = true;
	}
	if(e.code == "KeyD"){
		right = true;
	}

});

document.addEventListener("keyup", function(e){
	
	if(e.code == "KeyW"){
		up = false;
	}
	if(e.code == "KeyS"){
		down = false;
	}
	if(e.code == "KeyA"){
		left = false;
	}
	if(e.code == "KeyD"){
		right = false;
	}

});