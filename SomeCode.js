
var someVariable; // создание переменной

var a = 3;
var b = prompt();

var c, d, e;
var num = 3,
	str = "Some String";

3 + 5; // 8
"3" + "5"; // 35

// Числовой тип
5;
123.44;
3.42;
124566744;

// Строковый тип
"Some String";
'Another string';
"332"; // Тоже строка

// Логический (boolean) тип

true;
false;
3 > 2; // true
12 == 332; // false

// Объект
var obj = {};
var obj2 = {
	name: "John",
	surname: "Smith"
};



// Условный оператор if

if (condition) { // Если условие (condition) - истина, то
	// some code to do // я выполню код в фигурных скобках
} // иначе просто игнорирую тело оператора


if (condition) { // Если условие (condition) - истина, то
	// some code to do // я выполню код в фигурных скобках
} else { // иначе
	// выполнится код из этих фигурных скобок
}


// Циклы

while (condition) {

}


while(pass != "123"){
	pass = prompt("Введите пароль");
}

for(var i = 0; i <= 5; i++){
	result = result * i;
}

var user = {};

user.login = "admin";
user.pass = "Qwerty";
user.email = "some@email";

register(user);