let express = require('express');
let app = express();

let Twig = require('twig');

app.set("twig options", {
    allow_async: true
});

app.use(express.static('public'));

app.get('/', function(req, res){
	res.render('index.twig', {
		name: 'Alyosha'
	});
});

app.listen(8000, function(){
	console.log('Server started on port 8000');
});